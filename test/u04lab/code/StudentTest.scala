package u04lab.code

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test
import u04lab.code.Lists.List._

class StudentTest {

  val cPPS: Course = Course("PPS","Viroli")
  val cPCD: Course = Course("PCD","Ricci")
  val cSDR: Course = Course("SDR","D'Angelo")
  val s1: Student = Student("mario",2015)
  val s2: Student = Student("gino",2016)
  val s3: Student = Student("rino") // defaults to 2017

  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)

  @Test
  def testEnrolling(): Unit = {
    assertEquals(Cons("PCD",Cons("PPS",Nil())), s1.courses)
    assertEquals(Cons("PPS",Nil()), s2.courses)
    assertEquals(Cons("SDR",Cons("PCD",Cons("PPS",Nil()))), s3.courses)
  }

  @Test
  def testHasTeacher(): Unit = {
    assertTrue(s1.hasTeacher("Ricci"))
    assertFalse(s2.hasTeacher("Ricci"))
  }

}
