package u04lab.code

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertTrue}
import org.junit.jupiter.api.Test

class ComplexTest {

  val c1 = Complex(10,20)
  val c2 = Complex(1,1)

  @Test
  def testSumComplex(): Unit = {
    assertEquals(Complex(11,21), c1 + c2)
    assertEquals(c1, Complex(0,0) + c1)
  }

  @Test
  def testProductComplex(): Unit = {
    assertEquals(Complex(-10,30), c1 * c2)
    assertEquals(Complex(0,0), Complex(0,0) * c1)
  }

  @Test
  def testEquality(): Unit = {
    assertFalse(c1 == c2)
    assertTrue(Complex(1,1) == c2)
  }

  @Test
  def testToString(): Unit = {
    assertEquals("ComplexImpl(1.0,1.0)", c2.toString)
  }

}
