package u04lab.code

import Optionals._
import Optionals.Option._
import Streams._
import Streams.Stream._
import Lists.List._
import Lists.List

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

object PowerIterator {
  def apply[A](s: Stream[A]): PowerIterator[A] = PowerIteratorImpl(s)

  private case class PowerIteratorImpl[A](var s: Stream[A]) extends PowerIterator[A] {

    private var pastList: List[A] = Nil()

    override def next(): Option[A] = s match {
      case Stream.Cons(h,t) =>
        pastList = append(pastList, List.Cons(h(), Nil()))
        s = t()
        Option.Some(h())
      case _ => Option.empty
    }

    override def allSoFar(): List[A] = pastList

    override def reversed(): PowerIterator[A] = PowerIterator(toStream(reverse(pastList)))
  }
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(take(generate(Math.random() < 0.5))(size))
}
